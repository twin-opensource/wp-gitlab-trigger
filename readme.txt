=== Your Plugin Name ===
Contributors: snappytux
Tags: gitlab, pipeline
Tested up to: 6.3
Stable tag: 1.0.1
Requires PHP: 8.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A WordPress plugin to trigger Gitlab pipeline from Wordpress.

== Description ==

This plugin allows you to trigger a Gitlab pipeline from WordPress. To do this, you will need to create a personal_access_tokens in Gitlab and add the personal_access_tokens to the plugin settings. Any time you save a post or page in WordPress, the pipeline will be triggered.

== Installation ==

1. Upload the plugin folder to your /wp-content/plugins/ folder.
2. Go to the **Plugins** page and activate the plugin.

== Frequently Asked Questions ==

= How can I send feedback or get help with a bug? =

The best place to report bugs, feature suggestions, or any other (non-security) feedback is at <a href="https://gitlab.com/twin-opensource/wp-gitlab-trigger/-/issues">the WP Gitlab Trigger issues page</a>. Before submitting a new issue, please search the existing issues to check if someone else has reported the same feedback.

= Where can I report security bugs? =

The plugin contributors and WordPress community take security bugs seriously. We appreciate your efforts to responsibly disclose your findings, and will make every effort to acknowledge your contributions.

To report a security issue, please visit the [WordPress HackerOne](https://hackerone.com/wordpress) program.

== Screenshots ==
1. GitLab Trigger Pipeline Setting.

== Changelog ==

See the [release history](https://gitlab.com/twin-opensource/wp-gitlab-trigger/-/releases).

== Upgrade Notice ==

= 1.0 =

Init Plugin