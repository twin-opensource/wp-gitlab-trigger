# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.2] - 2023-09-14

### Changed

- Change plugin name
- Change `readme.txt` after [Readme validator](https://wordpress.org/plugins/developers/readme-validator/)

## [1.0.1] - 2023-09-14

### Added

- Add icons and banners of plugin

## [1.0.0] - 2023-09-13

### Added

- Init awesome plugin

[unreleased]: https://gitlab.com/twin-opensource/wp-gitlab-trigger/-/compare/dev...v1.0.2
[1.0.2]: https://gitlab.com/twin-opensource/wp-gitlab-trigger/-/compare/v1.0.1...v1.0.2
[1.0.1]: https://gitlab.com/twin-opensource/wp-gitlab-trigger/-/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/twin-opensource/wp-gitlab-trigger/-/releases/v1.0.0
