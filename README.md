# Gitlab Trigger

[![pipeline status](https://gitlab.com/twin-opensource/wp-gitlab-trigger/badges/dev/pipeline.svg)](https://gitlab.com/twin-opensource/wp-gitlab-trigger/-/commits/dev)
[![Latest Release](https://gitlab.com/twin-opensource/wp-gitlab-trigger/-/badges/release.svg)](https://gitlab.com/twin-opensource/wp-gitlab-trigger/-/releases)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=twin-opensource_wp-gitlab-trigger&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=twin-opensource_wp-gitlab-trigger)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=twin-opensource_wp-gitlab-trigger&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=twin-opensource_wp-gitlab-trigger)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=twin-opensource_wp-gitlab-trigger&metric=coverage)](https://sonarcloud.io/summary/new_code?id=twin-opensource_wp-gitlab-trigger)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=twin-opensource_wp-gitlab-trigger&metric=bugs)](https://sonarcloud.io/summary/new_code?id=twin-opensource_wp-gitlab-trigger)
[![License: GPL v2](https://img.shields.io/badge/License-GPL_v2-blue.svg)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)

Gitlab Trigger plugin for WordPress. [View on WordPress.org →](https://wordpress.org/plugins/gitlab-trigger/)

## Usage

See the [readme.txt](readme.txt) for installation and usage instructions.

## Contribute

Please [report (non-security) issues](https://gitlab.com/twin-opensource/wp-gitlab-trigger/-/issues) and [open merge requests](https://gitlab.com/twin-opensource/wp-gitlab-trigger/-/merge_requests) on Gitlab. See below for information on reporting potential security/privacy vulnerabilities.

When you're ready, open [a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork) with the suggested changes.

## Deployments

Deployments [to WP.org plugin repository](https://wordpress.org/plugins/gitlab-trigger/) are handled automatically by the Gitlab-ci [.gitlab-ci.yml](.gitlab-ci.yml.yml). All merges to the `main` branch are commited to the [`trunk` directory](https://plugins.trac.wordpress.org/browser/wp-gitlab-trigger/trunk) while all [Git tags](https://gitlab.com/twin-opensource/wp-gitlab-trigger/-/tags) are pushed as versioned releases [under the `tags` directory](https://plugins.trac.wordpress.org/browser/wp-gitlab-trigger/tags).

## Credits

Created [by contributors](https://gitlab.com/twin-opensource/wp-gitlab-trigger/-/project_members) and released under [GPLv2 or later](LICENSE.md).

## Security

Please privately report any potential security issues to the [WordPress HackerOne](https://hackerone.com/wordpress) program.
